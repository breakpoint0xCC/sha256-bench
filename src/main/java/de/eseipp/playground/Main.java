package de.eseipp.playground;


import java.io.IOException;

public final class Main {

    public static void main(String[] args) throws IOException {
        org.openjdk.jmh.Main.main(args);
    }

}
