package de.eseipp.playground;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

@Fork(value = 3, jvmArgsAppend = { "-XX:+UseZGC", "-XX:+AlwaysPreTouch", "-Xms512m", "-Xmx512m" })
@State(Scope.Benchmark)
public class JavaShaBenchmark {

    final byte[] data = "1234567890abcdefhijklmnopqrstuvwxyz".getBytes(StandardCharsets.UTF_8);
    final MessageDigest sha256;

    public JavaShaBenchmark() {
        try {
            this.sha256 = MessageDigest.getInstance("SHA-256");
        } catch (final Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Benchmark
    public void jvmSha256Calculation(final Blackhole blackhole) {
        blackhole.consume(sha256.digest(data));
    }

}
